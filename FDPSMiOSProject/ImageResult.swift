//
//  ImageResult.swift
//  FDPSMiOSProject
//
//  Created by Marco on 10/25/15.
//  Copyright © 2015 FH Technikum Wien. All rights reserved.
//

import Foundation

class ImageResult {
    
    private let imageUrl, imageName: String
    
    init(imageUrl: String, imageName: String) {
        self.imageUrl = imageUrl
        self.imageName = imageName
    }
    
    func getImageUrl() -> String {
        return imageUrl
    }
    func getImagename() -> String {
        return imageName
    }
}