//
//  ViewController.swift
//  FDPSMiOSProject
//
//  Created by Marco on 10/21/15.
//  Copyright © 2015 FH Technikum Wien. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    private var indicator:UIActivityIndicatorView? = nil
    private let cellIdentifier = "textCell"
    private var rowsData = [ImageResult]()
    private var urlToImage = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Image Search"
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: cellIdentifier)
        
        let headerView = SearchHeaderView.loadFromNib()
        headerView.searchButtonClicked = { [weak self] (word) in self?.searchButtonWasClicked(word) }
        tableView.tableHeaderView = headerView
        
        indicator = UIActivityIndicatorView  (activityIndicatorStyle: UIActivityIndicatorViewStyle.WhiteLarge)
        indicator!.color = UIColor.blackColor()
        indicator!.frame = CGRectMake(0, 0, 40, 40)
        indicator!.center = self.view.center
        self.view.addSubview(indicator!)
        indicator!.bringSubviewToFront(self.view)
    }
    
    override func viewDidLayoutSubviews() {
        sizeHeaderToFit()
    }

    private func sizeHeaderToFit() {
        // calculate min height manually, because there is no automatic dimension for TableHeaderViews
        guard let headerView = tableView.tableHeaderView else { return }
        headerView.setNeedsLayout()
        headerView.layoutIfNeeded()
        
        let minHeight = headerView.systemLayoutSizeFittingSize(UILayoutFittingCompressedSize).height
        
        headerView.frame.size.height = minHeight
        
        tableView.tableHeaderView = headerView
    }
    
    private func searchButtonWasClicked(var searchword: String) {
        searchword = searchword.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if searchword.characters.count <= 0 { return }
        
        rowsData.removeAll()
        self.tableView.reloadData()
        indicator?.startAnimating()
        
        
        Alamofire.request(.GET, "https://fhtw-google-image-search.herokuapp.com/ajax/services/search/images", parameters: ["v": "1.0", "rsz": "8", "q": searchword]).responseJSON { response in
            
            if let jsonString = response.result.value {
                let jsonObj = JSON(jsonString)
                guard let imageInfoArray = jsonObj["responseData"]["results"].array else { return }
                for imageInfo in imageInfoArray {
                    let url = imageInfo["url"].string ?? ""
                    let title = imageInfo["titleNoFormatting"].string ?? ""
                    self.rowsData.append(ImageResult(imageUrl: url, imageName: title))
                }
            }
            
            self.indicator?.stopAnimating()
            self.tableView.reloadData()
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        urlToImage = rowsData[indexPath.row].getImageUrl()
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        performSegueWithIdentifier("segueToImagePreviewVC", sender: self)
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowsData.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath)
        cell.textLabel?.text = rowsData[indexPath.row].getImagename()
        return cell
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "segueToImagePreviewVC" {
            guard let vc = segue.destinationViewController as? ImagePreviewViewController else { return }
            vc.imageUrl = self.urlToImage
        }
    }
}

