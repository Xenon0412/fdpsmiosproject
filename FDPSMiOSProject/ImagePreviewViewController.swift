//
//  ImagePreviewViewController.swift
//  FDPSMiOSProject
//
//  Created by Marco on 10/26/15.
//  Copyright © 2015 FH Technikum Wien. All rights reserved.
//

import UIKit
import Alamofire

class ImagePreviewViewController: UIViewController {
    
    @IBOutlet private weak var imgView: UIImageView!
    
    var imageUrl: String?
    
    override func viewDidLoad() {
        self.title = "Image"
        if imageUrl == nil { return }
        
        imgView.contentMode = .ScaleAspectFit
        
        let indicator = UIActivityIndicatorView  (activityIndicatorStyle: UIActivityIndicatorViewStyle.WhiteLarge)
        indicator.color = UIColor.blackColor()
        indicator.frame = CGRectMake(0, 0, 40, 40)
        indicator.center = self.view.center
        self.view.addSubview(indicator)
        indicator.bringSubviewToFront(self.view)
        
        indicator.startAnimating()
        
        Alamofire.request(.GET, imageUrl!).responseData { response in
            let data = response.data!
            self.imgView.image = UIImage(data: data)
            
            indicator.stopAnimating()
        }
    }
}