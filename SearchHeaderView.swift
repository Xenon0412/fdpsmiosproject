//
//  SearchHeaderView.swift
//  FDPSMiOSProject
//
//  Created by Marco on 10/24/15.
//  Copyright © 2015 FH Technikum Wien. All rights reserved.
//

import Foundation
import UIKit

class SearchHeaderView : UIView {
    
    @IBOutlet private weak var searchTextField: UITextField!
    @IBOutlet private weak var searchButton: UIButton!
    @IBAction private func searchButtonClicked(sender: AnyObject) {
        searchButtonClicked?(searchTextField.text!)
    }
    
    var searchButtonClicked: ((String) -> ())?
    
    class func loadFromNib() -> SearchHeaderView {
        return NSBundle.mainBundle().loadNibNamed("SearchHeaderView", owner: nil, options: nil)[0] as! SearchHeaderView
    }
}